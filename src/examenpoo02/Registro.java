/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenpoo02;

/**
 *
 * @author ionix
 */
public class Registro {
    private Bono Hijos;
    private int numDocente;
    private String nombre;
    private String domicilio;
    private int nivel;
    private int horas;
    private int pagoHora;
    
    public Registro(){
        
    }

    public Registro(int numDocente, String nombre, String domicilio, int nivel, int horas) {
        this.numDocente = numDocente;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.nivel = nivel;
        this.horas = horas;
    }
    
    public Registro(Registro otro){
        this.numDocente = otro.numDocente;
        this.nombre = otro.nombre;
        this.domicilio = otro.domicilio;
        this.nivel = otro.nivel;
        this.horas = otro.horas;
    }

    public int getNumDocente() {
        return numDocente;
    }

    public void setNumDocente(int numDocente) {
        this.numDocente = numDocente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public int getHoras() {
        return horas;
    }

    public void setHoras(int horas) {
        this.horas = horas;
    }
    
    public float calcularPagoHora(){
        float pagoHora = 0.0f;
        
        return pagoHora;
    }

    public int getPagoHora() {
        return pagoHora;
    }

    public void setPagoHora(int pagoHora) {
        this.pagoHora = pagoHora;
    }
    
    
    
    public float calcularPagoBono(){
        float pagoBono = 0.0f;
        switch(this.nivel){
            case 0: pagoBono = (this.pagoHora * 1.3f)*this.horas; break;
            case 1: pagoBono = (this.pagoHora * 1.5f)*this.horas; break;
            case 2: pagoBono = (this.pagoHora * 2f)*this.horas; break;
        }
        return pagoBono;
    }
    
    public float calcularImpuesto(){
        float impuesto = 0.0f;
        impuesto = this.calcularPagoBono() * 0.16f;
        return impuesto;
    }
    
    public float calcularTotalPagar(int numHijos){
        float totalPagar = 0.0f;
        totalPagar = this.calcularPagoBono() - this.calcularImpuesto() + this.calcularBono(numHijos);
        return totalPagar;
    }
    
    public float calcularBono(int numHijos){
        float bono=0.0f;
          if(numHijos>0 && numHijos<3) bono = this.calcularPagoBono()*0.05f;
          if(numHijos>2 && numHijos<6) bono = this.calcularPagoBono()*0.1f;
          if(numHijos>5) bono = this.calcularPagoBono()*0.2f;
        return bono;
    }

}
